'use strict';


export class Clase{
	constructor(){
		this.sum = function(a, b){
			return a + b; 
		}
		this.rest = function(a, b){
			return a - b; 
		}
		this.mult = function(a, b){
			return a * b; 
		}
		this.div = function(a, b){
			return a / b; 
		}
	}	
	getSum(x, y){
		return this.sum(x, y);
	}
	getRest(x, y){
		return this.rest(x, y);
	}
	getMult(x, y){
		return this.mult(x, y);
	}
	getDiv(x, y){
		return this.div(x, y);
	}
};