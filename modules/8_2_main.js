'use strict';
import {Clase} from './8_1_class.js';
import {obj} from './8_0_object.js';

const objClase = new Clase();
let result = objClase.getSum(1, 2);
console.log(result);
result = obj.sum(1, 2);
console.log(result);