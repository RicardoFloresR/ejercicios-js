'use strict';
export const obj = {
    sum : function(a, b){
        return a + b;
    },
    rest : function(a, b){
        return a - b;
    },
    mult : function(a, b){
        return a * b;
    },
    div : function(a, b){
        return a / b;
    }
}
// export default obj;