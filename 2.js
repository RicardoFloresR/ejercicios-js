'use strict';

// 3. Get access to global scope on getName subfunction

this.name = 'global';
var module = {
  name: 'local',
  getName: function(){
  	return this.name;
  }
};

console.log(module.getName.call(this)); // Should be global

// 4. Get access to global var to get the result

var a = 2;

(function IIFE(b) {
  var a = 3;
  var result = a + b; // Sum the local var a with global var a;
  console.log(result); // Should be 5;
})(a);

/*****************************************
* YEAH!! YOU STILL BE HERE YET
*****************************************/

// 5. Convert and resolve the request using a callback

var result = 0;

function getValue (number, callback) {
  return callback(number);
};

result = getValue(2, function(number){
	return number + 3;
});
console.log(result) // Should be 5;