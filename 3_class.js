'use strict';
class evento{
	constructor(lugar, anfitrion){
		this.lugar = lugar;
		this.anfitrion = anfitrion;
	};
};
const meetup = class extends evento{
	detalles(){
		console.log('El meetup de ' + this.anfitrion + ' será en ' + this.lugar + '.');
	};
};
let meetup_EventLoop = new meetup('las oficinas de Platzi México', 'EventLoop');
let meetup_AngularMex = new meetup('Central', 'Angular México');
meetup_EventLoop.detalles();
meetup_AngularMex.detalles();


let gato = function(a, b){
	return a + b;
}