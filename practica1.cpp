#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;



//Clase Ser Vivo - Primer nivel
class ser_vivo{
public:
	void omeaostasis(){
		cout<<"Omeostasis"<<endl;
	}
	void crecimiento(){
		cout<<"Aumentar tamaño"<<endl;
	}
	void morir(){
		cout<<"Desactivar todas las funciones"<<endl;
	}
	void setVida(int num){
		diasVida = num;
	}
	void mostrarVida(){
		cout<<"Este animal vive aproximadamente "<<diasVida<<" dias"<<endl;
	}
private:
	int diasVida;
	string nombre;
};


//Clase animal - Segundo nivel
class animal : public ser_vivo{
public:
	void buscarAlimento(){
		cout<<"Buscar alimento"<<endl;
	}	
	void generarCelulas(){
		cout<<"Generar celulas"<<endl;
	}
	void desplazar(){
		cout<<"Moviendo"<<endl;
	}
};

//Clase Vertebrado - Tercer nivel
class vertebrado : public ser_vivo{
public:
	void setNumHuesos(int num){
		numeroHuesos = num;
	}
	void mostrarNumHuesos(){
		cout<<"Este animal vertebrado tiene "<<numeroHuesos<< " huesos"<<endl;
	}
private:
	int numeroHuesos;
};

//Clase Mamiferos - Cuarto niviel
class mamiferos : public vertebrado{
public:
	 void setgestacion(int dias){
		gestacion  = dias;
	}
	 void mostrarGestacion(){
	 	cout<<"Este animal mamifero tarda "<<gestacion<<" dias en nacer"<<endl;
	 }
private:
	int gestacion;	
};

//Clase Carnivoro - Quinto nivel 
class carnivoro : public mamiferos{
public:
	void setKilosCarne(int num){
		kilosCarne = num;
	}
private: 
	int kilosCarne;
};

//Clase perro - Sexto nivel 
class perro : public carnivoro{
public:
	void setRaza(string nombre){
		raza = nombre;
	}
	void setColor(string c){
		color = c;
	}
	void mostrarDatos(){
		cout<<"Este perro es de raza "<< raza <<endl;
		cout<<"Este perro es de color "<< color <<endl;
	}

private:
	int numPatas = 4;
	string color;
	string raza;
};

//Clase rottweiler - ULTIMO NIVEL

class rottweiler : public perro{
public:
	rottweiler(int vida, int hues, int gest, int carne, int kg, string r, string c){
		setVida(vida);
		setNumHuesos(hues);
		setgestacion(gest);
		setPeso(kg);
		setRaza(r);
		setColor(c);
	}
	void setPeso(int num){
		peso = num;
	}
	void mostrarInfo(){
		mostrarVida();
		mostrarNumHuesos();
		mostrarGestacion();
		mostrarDatos();
		cout<<"Este rottweiler tiene un peso de "<<peso<<" kg"<<endl;
	}
private:
	int peso;
};

int main(void)
{
	rottweiler rottweiler1(3000, 321, 68, 3, 80, "Rottweiler", "Negro");
	rottweiler1.mostrarInfo();
		return 0;
}