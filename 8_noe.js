'use strict';

// ------------------------
// EJERCICIO 1
// ------------------------
// (function f(f){
// 	return typeof f(); //Retorna number
// })(function(){return 1});

// ------------------------
// EJERCICIO 2
// ------------------------
// let bar = (a) => a*2;
// let foo = (function(){
// 	let a = Math.abs(12.5);
// 	return function(b){
// 		if(a >= Math.PI * 2){
// 			return function(x){
// 				console.log(x);
// 			}
// 		}
// 		else{
// 			return () => Math.PI * 2;
// 		}
// 	}
// })(15);

// console.log(foo('a')); //foo sera una funcion que retornara bar(Math.PI)

// ------------------------
// EJERCICIO 3
// ------------------------
// let bar = [
// 	{id : 1, name : 'Ricardo', username : 'Cerdito'},
// 	{id : 2, name : 'Ervin', username : 'Antonette'}
// ];

// let foo = {
// 	userId : 1,
// 	id : 1,
// 	title : "Dev",
// 	completed : false
// };

// let {name} = bar;
// let {title, completed} = foo;

// console.log(name, title);

// ------------------------
// EJERCICIO 4
// ------------------------
// function foo(a, b){
// 	arguments[1] = 2;
// 	alert(b);
// }
// foo(1);

// ------------------------
// EJERCICIO 5
// ------------------------
// var x = 5;
// var y = 6;
// var res = eval("x*y");
// console.log(res);

// ------------------------
// EJERCICIO 6
// ------------------------
// function bar(){
// 	return foo;
// 	foo = 10;
// 	function foo(){};
// 	var foo = '11';
// };
// alert(typeof bar());

// ------------------------
// EJERCICIO 7
// ------------------------
// var x = 3;
// var foo = {
// 	x : 2,
// 	baz : {
// 		x : 1,
// 		bar : function(){
// 			return this.x;
// 		}
// 	}
// };
// var go = foo.baz.bar;
// console.log(go());
// console.log(foo.baz.bar());

// ------------------------
// EJERCICIO 8
// ------------------------
// var bar = 1, foo = {};
// foo : {
// 	bar : 2,
// 	baz : ++bar,
// };
// console.log(foo.baz + foo.bar + bar);

// ------------------------
// EJERCICIO 9
// ------------------------
// var a = 'GeeksForGeeks';
// var result = a.substring(4, 5);
// console.log(result);

// ------------------------
// EJERCICIO 10
// ------------------------
// var miPersonaFavorita = {
// 	name : 'Ana',
// 	apellido : 'Juarez'
// };
// const miPersonajeFavorita = {
// 	name : 'Ana',
// 	apellido : 'Juarez'
// };
// let miAmigoFavorito = {
// 	name : 'Mary',
// 	apellido : 'Juarez'
// };
// miAmigoFavorito = miPersonaFavorita == miPersonajeFavorita;
// miPersonaFavorita.name = 'Ana';
// try{
// 	miAmigoFavorito.name = 'Mary';
// }catch(e){
// 	console.log('Error: ', e)
// }
// miPersonajeFavorita.name = 'Mary';

// console.log(!!! miPersonaFavorita === miAmigoFavorito);
// console.log(miPersonajeFavorita.name == miPersonaFavorita.name);

// ------------------------
// EJERCICIO 10
// ------------------------
var arr = [];
arr[0] = 'a';
arr[1] = 'b';
arr.foo = 'c';
console.log(arr.length);
console.log(Object.getOwnPropertyNames(arr));


















