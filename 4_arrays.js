// 'use strict';

const methods = [
    {method: 'filter', easyToUse: 'easy'},
    {method: 'map', easyToUse: 'easy'},
    {method: 'reduce', easyToUse: 'hard'},
    {method: 'find', easyToUse: 'easy'},
    {method: 'every', easyToUse: 'easy'},
];
// 1
methods.push({method : 'fill', easyToUse : 'easy'});
console.log(methods);

// 2
let easyMethods = methods.filter(objeto => objeto.easyToUse == 'easy');
console.log(easyMethods);

// 3
let nonHardMethods = methods.filter(objeto => objeto.easyToUse != 'hard');
console.log(nonHardMethods);

// 4

let easyToTrue = methods.map(objeto => {
	// let aux = {...objeto};
	// if(aux.easyToUse === 'easy'){
	// 	aux.easyToUse = true;
	// }
	// return aux;
	return {
		...objeto,
		easyToUse : objeto.easyToUse != 'hard'
	}
});
console.table(easyToTrue);

// 5
// let hardToFalse = (methods.slice()).map(objeto => {
// 	let aux = {...objeto};
// 	if(aux.easyToUse === 'hard'){
// 		aux.easyToUse = false;
// 	}
// 	return aux;
// });
// console.log(hardToFalse);

// 6
let addIndex = methods.map(function(objeto, index){
	return {
		id: index,
		...objeto
	}
});
console.log(addIndex);
