'use strict';
function go(n) {
  // n here is defined!
  console.log(n); // Object {a: [1,2,3]}
  for (var n of n.a) { // ReferenceError
    console.log(n);
  };
};

go({a: [1, 2, 3]});
/*declarar un array, comparar contra un objeto 
console.log(typeof array)
console.log(comparativa de typèof). Hacer que salga false */

const array = ['a', 'b'];
const array2 = [1, 2];
const obj = {};
console.log(array2.__proto__ === array.__proto__);
console.log(obj.__proto__ === array.__proto__);

// console.log(array.__proto__)

let clone = array;
array[0] = 'z';
console.log(clone, array);


let arrayCopy = [...array]; // create TRUE copy

arrayCopy[0] = '👻';
console.log(arrayCopy); 
console.log(array);	

let i = 0;
function recur(a){
	console.log('Hola x' + a);
	a++;
	a == 10 ? console.log('fin') : recur(a);
};

recur(i);