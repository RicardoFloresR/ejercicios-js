// A partir de dos objetos, construye uno solo el cual comparta prototipos de ambos, es decir, 
// que el objeto final resultante debe tener en su cadena de prototipo las propiedades de ambos objetos, 
// los objetos son los siguientes:
'use strict';

const person = function(){
  this.eyes = 'brown',
  this.hair = 'black',
  this.skin = 'white',
  this.sex = 'female',
  this.name = 'Brenda'
};

const developer = function() {
  this.lang = 'Javascript',
  this.type = 'frontend'
};

// developer.prototype = new person;

const Brenda = new developer();
// Brenda.prototype = new person;
Object.setPrototypeOf(Brenda, new person);
console.log(Brenda.eyes);

for (let item in Brenda){
	console.log(item);
}


console.log(Object.getOwnPropertyNames(Brenda));