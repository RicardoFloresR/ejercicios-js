'use strict';


let user = {
	name : 'John',
	age : 30,
	sayHi : function(){
		return console.log('Javascript is awesome!!');
	}
};

user.sayHi(user);


let user1 = {
	name : 'John',
	age : 30,
	sayName : function(){
		return console.log(this.name);
	}
};

user1.sayName();

let user3 = { name : 'John' };
let admin = { name : 'Admin' };

function sayHi(){
	alert(this.name);
};

// Qué se debe de ver si se ejecuta 'user.f y admin.f' ??
// Respuesta: Se deberia de ver un error ya que no existe pero podriamos agregarlo con las siguientes lineas antes de ejecutar user.f y admin.f:
//  user3.f = function(){ return console.log('it works from user3!!') };
//  user4.f = function(){ return console.log('it works from admin!!') };

let user5 = {
	name : 'John from user5',
	go : function(){
		console.log(this.name);
	}
};

// (user5.go)();
//Que se deberia ver en la consola?? 
// Respuesta: Un error en el que se marca una referencia a un objeto que aun no ha sido creado o instanciado ya que las IIFE
// se ejecutan durante el momento de declaracion y en ese momento el objeto user5 solo ha sido declarado pero no creado

function makeUser(){
	return {
		name : 'John from makeUser',
		ref : this
	}
};

let user6 = makeUser();

// console.log(user6.ref().name);

// Que se debe ver en la consola
// Respuesta: Se deberia de ver 'John' ya que al asignar this a ref se le esta asignando el contexto del objeto que lo contiene
// Correcion: No se ve nada ya que this en ese momento en undefined, la unica solucion en tranformar la funcion makeUser en la forma siguiente
// function makeUser(){
// 	return {
// 		name : 'John from makeUser',
// 		ref : function(){
// 			return this;
// 		}
// 	}
// }


// function temasVistos(temas, callback){
// 	// Qué falta aquí 
// }

// temasVistos(/*Aquí*/,  (cuales) => {
// 	// y aquí
// }

// Para que imprima lo siguiente:
// this, argument, callbacks, iife

// Solucion: 
function temasVistos(temas, callback){
	callback(temas);
};

temasVistos(['this', 'callback', 'arguments', 'IIFE'], (cuales) =>{
	let result = cuales.join(', ');
	console.log(result);
});

