'use strict';
const keys = [];
array.forEach(item => {
	let name = item.modulo.nombre;
	let keyName = name.slice(5, name.length - 6);
	if(!keys.includes(keyName) && keyName.length != 0) keys.push(keyName);
});

const newArray = keys.map(key => {
	return {
		nombre_menu : key,
		id_menu : `${key}FAC`,
		options : (array.filter(item => item.modulo.nombre.includes(key))).map(item => item.modulo)
	};
});
console.log(newArray);
